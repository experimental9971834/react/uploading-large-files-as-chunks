import axios from "axios";
import PQueue from "p-queue";
import { useState } from "react";
import { v4 as uuidv4 } from "uuid";

const CHUNK_SIZE = 1024 * 1024 * 5; // 5MB
const MAX_CONCURRENT_UPLOADS = 6;
const MAX_RETRY_COUNT = 3;
const RETRY_DELAY = 1000; // 1 second

const uploadQueue = new PQueue({ concurrency: MAX_CONCURRENT_UPLOADS });

export const useChunkedUpload = () => {
  const [FileNumber, setFileNumber] = useState(0);
  const [showProgress, setShowProgress] = useState(false);
  const [filesToBeUpload, setFilesToBeUpload] = useState<File[]>([]);

  const uploadCompleted = async (guid: string, total: number) => {
    const response = await axios.post(
      "http://my-fans-backend.test/api/article/upload-complete/",
      undefined,
      {
        params: {
          file_name: guid,
          total_chunks: total,
        },
        headers: {
          Accept: "application/json",
          Authorization:
            "Bearer " + "29|AysDzlGcXtnAlTHX51g6vLc6uhLRTQHQT5J7V6Kf",
        },
      }
    );
    const data = response.data;
    if (data.success) console.log("Upload Completed", guid);
  };

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) return;
    const _files = Array.from(e.target.files);
    setFilesToBeUpload(_files);
  };

  const uploadChunk = async (
    fileGuid: string,
    chunk: FormData,
    counter: number
  ) => {
    let retryCount = 0;

    while (retryCount < MAX_RETRY_COUNT) {
      try {
        const response = await axios.post(
          "http://my-fans-backend.test/api/article/upload-chunk/",
          chunk,
          {
            params: {
              id: counter,
              file_name: fileGuid,
            },
            headers: {
              "Content-Type": "multipart/form-data",
              Accept: "application/json",
              Authorization:
                "Bearer " + "29|AysDzlGcXtnAlTHX51g6vLc6uhLRTQHQT5J7V6Kf",
            },
          }
        );

        const data = response.data;
        if (data.success) {
          console.log("Chunk Uploaded Successfully", counter);
          return;
        } else {
          console.log("Error Occurred:", data.error_message);
        }
      } catch (error) {
        console.log("error", error);
        console.log("Retrying in 1 second");
        await new Promise((resolve) => setTimeout(resolve, RETRY_DELAY));
        retryCount++;
      }
    }
  };

  const handleFileUpload = async (file: File) => {
    const fileGuid = uuidv4() + "." + file.name.split(".").pop();
    const _totalCount =
      file.size % CHUNK_SIZE === 0
        ? file.size / CHUNK_SIZE
        : Math.floor(file.size / CHUNK_SIZE) + 1; // Total count of chunks will have been upload to finish the file

    const chunkPromises = Array<Promise<void>>();
    for (let counter = 1; counter <= _totalCount; counter++) {
      const start = (counter - 1) * CHUNK_SIZE;
      const end = counter * CHUNK_SIZE;
      const chunk = file.slice(start, Math.min(end, file.size));

      const formData = new FormData();
      formData.append("file", chunk);

      const promise = uploadQueue.add(
        async () => await uploadChunk(fileGuid, formData, counter)
      );
      chunkPromises.push(promise);
    }

    await Promise.allSettled(chunkPromises);
    console.log("All chunks uploaded");
    await uploadCompleted(fileGuid, _totalCount);
  };

  const handleUploadToAll = async () => {
    if (filesToBeUpload.length <= 0) return;

    setShowProgress(true);
    for (const file of filesToBeUpload) {
      setFileNumber((prev) => prev + 1);
      await handleFileUpload(file);
    }
    setShowProgress(false);
    setFilesToBeUpload([]);
  };

  return {
    handleFileChange,
    handleUploadToAll,
    showProgress,
    current: FileNumber,
    total: filesToBeUpload.length,
  };
};
