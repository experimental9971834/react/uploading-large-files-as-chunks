import { useChunkedUpload } from "./hooks/useChunkedUpload";

function App() {
  const { showProgress, current, total, handleFileChange, handleUploadToAll } =
    useChunkedUpload();

  const progress = Math.round((current / total) * 100);

  return (
    <main className="container mx-auto space-y-8">
      <h1 className="text-3xl font-bold">Uploading Large Files as Chunks</h1>

      {showProgress ? (
        <div className="w-full rounded-full">
          <p>
            {current} of {total} files uploading
          </p>
          <div
            className="bg-blue-600 text-xs font-medium text-blue-100 text-center p-0.5 leading-none rounded-full transition-all animate-pulse"
            style={{
              width: `${progress}%`,
              display: showProgress ? "block" : "none",
            }}
          >
            {progress}%
          </div>
        </div>
      ) : null}
      <form className="space-y-4">
        <label className="block">
          <span className="sr-only">Choose file</span>
          <input
            multiple
            type="file"
            name="file"
            onChange={handleFileChange}
            className="block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-md file:border-0 file:text-sm file:font-semibold file:bg-blue-500 file:text-white hover:file:bg-blue-600"
          />
        </label>
        <button
          type="button"
          onClick={handleUploadToAll}
          className="w-full py-3 px-4 inline-flex justify-center items-center gap-2 rounded-md border border-transparent font-semibold bg-blue-500 text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 transition-all text-sm"
        >
          Upload File
        </button>
      </form>
    </main>
  );
}

export default App;
